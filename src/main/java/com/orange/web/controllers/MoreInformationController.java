package com.orange.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/web")
public class MoreInformationController {


    @GetMapping("/more-information")
    public String moreInformationPage(@RequestParam(value = "totalCreditScore") Double totalCreditScore, Model model) {
        model.addAttribute("totalCreditScore", "Credit score: " + totalCreditScore);
        model.addAttribute("typeOfCreditScore", typeOfCreditScore(totalCreditScore));
        return "more_information";
    }

    private String typeOfCreditScore(Double totalCreditScore) {
        String type = "";
        if (totalCreditScore < 450.0) {
            type = "Poor";
        } else if (totalCreditScore >= 450.0 && totalCreditScore < 550.0) {
            type = "Below average";
        } else if (totalCreditScore >= 550.0 && totalCreditScore < 650.0) {
            type = "Medium";
        } else if (totalCreditScore >= 650.0 && totalCreditScore < 750.0) {
            type = "Good";
        } else if (totalCreditScore > 750.0) {
            type = "Impressive";
        }
        return type;
    }
}
